import React, { Component } from 'react';
import { View } from 'react-native';
import firebase from 'firebase';
import firestore from 'firebase/firestore';
import { Header, Button, Spinner, Card, CardSection } from './components/common/index';
import LoginForm from './components/LoginForm';

class App extends Component {
    state = { loggedIn: null }

    componentWillMount() {
        const fs = firebase.initializeApp({
            apiKey: 'AIzaSyDw53Btwy6KInl7_baCfY992OBsMD018N8',
            authDomain: 'simplechat-fe9d3.firebaseapp.com',
            databaseURL: 'https://simplechat-fe9d3.firebaseio.com',
            projectId: 'simplechat-fe9d3',
            storageBucket: 'simplechat-fe9d3.appspot.com',
            messagingSenderId: '566746401594'
        });

        

        // const timestamp = snapshot.get('created_at');
        // const date = timestamp.toDate();

        const firestore = fs.firestore();
        const settings = {/* your settings... */ timestampsInSnapshots:true};
        firestore.settings(settings);

        firebase.auth().onAuthStateChanged((user) => {
            
            if (user) {
                this.setState({ loggedIn: true });
                firestore.collection("users").add({
                    first: "Ada",
                    last: "Lovelace",
                    born: 1815
                })
                .then(function(docRef) {
                    console.log("Document written with ID: ", docRef.id);
                })
                .catch(function(error) {
                    console.error("Error adding document: ", error);
                });
            } else {
                this.setState({ loggedIn: false });
            }
        });






 
    }



    

    renderContent() {
        switch (this.state.loggedIn) {
            case true:
                return (
                    <Card>
                        <CardSection>
                            <Button>
                                All Users
                          </Button>

                            <Button>
                                Chat Room
                          </Button>

                        </CardSection>

                        <CardSection>
                            <Button onPress={() => firebase.auth().signOut()}>
                                Log Out
              </Button>
                        </CardSection>
                    </Card>
                );
            case false:
                return <LoginForm />;
            default:
                return <Spinner size="large" />;
        }
    }


    render() {
        return (
            <View>
                <Header headerText="Welcome to SimpleChat!" />
                {this.renderContent()}
            </View>
        );
    }
}

export default App;
